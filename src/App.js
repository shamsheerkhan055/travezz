import "./App.css";
import axios from "axios";
import React, { useState, useEffect } from "react";
import {NavLink,Navbar,Nav,Form,Container,Row,Col,NavDropdown,} from "react-bootstrap";
import {Link } from 'react-router-dom';
import { BrowserRouter, Route, Routes } from "react-router-dom";
import Login from "./Components/Login";
import Home from "./Home";
import Blog from "./Components/Blog";
import About from "./Components/About";
import ContactUS from "./Components/ContactUS";
import Andhra from "./Components/Andhra";
import AP from "./Components/AP";



function App(args) {
  return (
    
    <div className="bg-color cursor-pointer">
      <Navbar expand="lg" className="bg-black text-white gap-5 fixed-top">
        <Container>
          <svg
            xmlns="http://www.w3.org/2000/svg"
            width="38"
            height="38"
            fill="currentColor"
            class="bi bi-globe"
            viewBox="0 0 16 16"
          >
            <path d="M0 8a8 8 0 1 1 16 0A8 8 0 0 1 0 8m7.5-6.923c-.67.204-1.335.82-1.887 1.855A7.97 7.97 0 0 0 5.145 4H7.5zM4.09 4a9.267 9.267 0 0 1 .64-1.539 6.7 6.7 0 0 1 .597-.933A7.025 7.025 0 0 0 2.255 4zm-.582 3.5c.03-.877.138-1.718.312-2.5H1.674a6.958 6.958 0 0 0-.656 2.5zM4.847 5a12.5 12.5 0 0 0-.338 2.5H7.5V5zM8.5 5v2.5h2.99a12.495 12.495 0 0 0-.337-2.5zM4.51 8.5a12.5 12.5 0 0 0 .337 2.5H7.5V8.5zm3.99 0V11h2.653c.187-.765.306-1.608.338-2.5zM5.145 12c.138.386.295.744.468 1.068.552 1.035 1.218 1.65 1.887 1.855V12zm.182 2.472a6.696 6.696 0 0 1-.597-.933A9.268 9.268 0 0 1 4.09 12H2.255a7.024 7.024 0 0 0 3.072 2.472M3.82 11a13.652 13.652 0 0 1-.312-2.5h-2.49c.062.89.291 1.733.656 2.5zm6.853 3.472A7.024 7.024 0 0 0 13.745 12H11.91a9.27 9.27 0 0 1-.64 1.539 6.688 6.688 0 0 1-.597.933M8.5 12v2.923c.67-.204 1.335-.82 1.887-1.855.173-.324.33-.682.468-1.068zm3.68-1h2.146c.365-.767.594-1.61.656-2.5h-2.49a13.65 13.65 0 0 1-.312 2.5zm2.802-3.5a6.959 6.959 0 0 0-.656-2.5H12.18c.174.782.282 1.623.312 2.5zM11.27 2.461c.247.464.462.98.64 1.539h1.835a7.024 7.024 0 0 0-3.072-2.472c.218.284.418.598.597.933zM10.855 4a7.966 7.966 0 0 0-.468-1.068C9.835 1.897 9.17 1.282 8.5 1.077V4z" />
          </svg>
          <Navbar.Brand>
            {" "}
            <img
              src="https://mail.google.com/mail/u/0?ui=2&ik=c6587d1e94&attid=0.1&permmsgid=msg-f:1783892230159262200&th=18c1a8882838e1f8&view=att&disp=safe&realattid=f_lpjlmeli0"
              alt="khan"
              style={{ width: "200px" }}
            />{" "}
          </Navbar.Brand>
          <Navbar.Toggle
            aria-controls="basic-navbar-nav"
            style={{ color: "white" }}
          />
          <Navbar.Collapse id="basic-navbar-nav">
            <div className="d-flex justify-content-around">
              <div className="d-flex justify-content-around">
                <Nav className="me-auto bs-light text-white mx-5 gap-30">
                  <Nav.Link href="/" className=" text-white margin-left:5px">
                    HOME
                  </Nav.Link>
                  <Nav.Link href="/Blogs" className=" text-white">
                    BLOG
                  </Nav.Link>
                  <Nav.Link href="/About" className=" text-white">
                    ABOUT
                  </Nav.Link>
                  <Nav.Link href="/Contact" className=" text-white">
                    CONTACT
                  </Nav.Link>
                </Nav>
              </div>
            </div>
            <div className="me-auto mx-5 gap-50">
              <NavDropdown title="STATES" id="basic-nav-dropdown" data-boundary='viewport'>
                <NavDropdown.Item href="/ANDHRA">ANDHRA PRADESH</NavDropdown.Item>
                <NavDropdown.Item href="/AP">
                  ARUNACHAL PRADESH
                </NavDropdown.Item>
                <NavDropdown.Item href="/ASSAM">
                  ASSAM
                </NavDropdown.Item>
                <NavDropdown.Item href="/BIHAR">
                  BIHAR
                </NavDropdown.Item>
                <NavDropdown.Item href="/CHHATTISGARH">
                  CHHATTISGARH
                </NavDropdown.Item>
                <NavDropdown.Item href="/GOA">
                  GOA
                </NavDropdown.Item>
                <NavDropdown.Item href="/GUJRAT">
                  GUJRAT
                </NavDropdown.Item>
                <NavDropdown.Item href="/HARYANA">
                  HARYANA
                </NavDropdown.Item>
                <NavDropdown.Item href="/HIMACHAL PRADESH">
                  HIMACHAL PRADESH
                </NavDropdown.Item>
                <NavDropdown.Item href="/JAMMU KASHMIR">
                  JAMMU KASHMIR
                </NavDropdown.Item>
                <NavDropdown.Item href="/JHARKHAND">
                  JHARKHAND
                </NavDropdown.Item>
                <NavDropdown.Item href="/KARNATAKA">
                  KARNATAKA
                </NavDropdown.Item>
                <NavDropdown.Item href="/KERALA">
                  KERALA
                </NavDropdown.Item>
                <NavDropdown.Item href="/MADHYA PRADESH">
                  MADHYA PRADESH
                </NavDropdown.Item>
                <NavDropdown.Item href="/MAHARASHTRA">
                  MAHARASHTRA
                </NavDropdown.Item>
                <NavDropdown.Item href="/MANIPUR">
                  MANIPUR
                </NavDropdown.Item>
                <NavDropdown.Item href="/MEGHALAYA">
                  MEGHALAYA
                </NavDropdown.Item>
                <NavDropdown.Item href="/MIZORAM">
                  MIZORAM
                </NavDropdown.Item>
                <NavDropdown.Item href="/NAGALAND">
                  NAGALAND
                </NavDropdown.Item>
                <NavDropdown.Item href="/ODISHA">
                  ODISHA
                </NavDropdown.Item>
                <NavDropdown.Item href="/PUNJAB">
                  PUNJAB
                </NavDropdown.Item>
                <NavDropdown.Item href="/RAJASTAN">
                  RAJASTAN
                </NavDropdown.Item>
                <NavDropdown.Item href="/Contact">
                </NavDropdown.Item>
              </NavDropdown>
            </div>
            <div className=" me-auto  gap-30">
                <NavDropdown title="SEASONS" id="basic-nav-dropdown">
                  <NavDropdown.Item href="Contact">SUMMER</NavDropdown.Item>
                  <NavDropdown.Item href="Contact">
                    SPRING
                  </NavDropdown.Item>
                  <NavDropdown.Item href="Contact">
                    MONSOON
                  </NavDropdown.Item>
                  <NavDropdown.Item href="Contact">
                    WINTER
                  </NavDropdown.Item>
                </NavDropdown>
            </div>

          </Navbar.Collapse>
          <Form inline>
            <Row>
              {/* <Col xs="auto">
          <Form.Control
            type="text"
            placeholder="Search"
            className=" mr-sm-2"
          />
        </Col> */}
              <Col xs="auto"></Col>
            </Row>
          </Form>
          <Nav className="ml-auto flex-column display:flex" navbar>
            <NavLink>
              <Login />
            </NavLink>
          </Nav>
        </Container>
      </Navbar>
      <br />
      <br />
      <br />
      
      <div>
        <BrowserRouter>
          <Routes>
            <Route path="/" element={<Home />} />
            <Route path="/Blogs" element={<Blog />} />
            <Route path="/About" element={<About />} />
            <Route path="/Contact" element={<ContactUS />} />
            <Route path="/Andhra" element={<Andhra/>} />
            <Route path="/AP" element={<AP/>} />
          </Routes>
        </BrowserRouter>
      </div><br/><br/>
      <div className='footer text-center text-black bg-white'>
      

    {/* <!-- Section: Form --> */}
    <section class="">
      <form action="">
        {/* <!--Grid row--> */}
        <div class="row d-flex justify-content-center">
          {/* <!--Grid column--> */}
          <div class="col-auto">
            <p class="pt-2">
              <strong>Sign up for our newsletter</strong>
            </p>
          </div>
          {/* <!--Grid column--> */}

          {/* <!--Grid column--> */}
          <div class="col-md-5 col-12">
            {/* <!-- Email input --> */}
            <div data-mdb-input-init class="form-outline mb-4">
              <input type="email" id="form5Example24" class="form-control" />
              <label class="form-label" for="form5Example24">Email address</label>
            </div>
          </div>
          {/* <!--Grid column--> */}

          {/* <!--Grid column--> */}
          <div class="col-auto">
            {/* <!-- Submit button --> */}
            <button data-mdb-ripple-init type="submit" class="btn btn-outline-black  mb-4" >
              Subscribe
            </button>
          </div>
          {/* <!--Grid column--> */}
        </div>
        {/* <!--Grid row--> */}
      </form>
    </section>
          <p>Search | Terms of services </p>
          <p>Copyright: © 2023 TrevEZ!</p>
          <p>Powerd by Shamsheer khan</p>
        </div>
    </div>
  );
}

export default App;

