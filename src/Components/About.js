import React from 'react'
import Card from 'react-bootstrap/Card';
import {
  Navbar,
  NavbarBrand,
  Nav,
  NavLink,
  CardGroup,
  Button
  } from 'reactstrap';

function About() {
  return (
    <section>
      <div>
        <CardGroup>
        <Card>
  <Card.Img variant="top" src="https://images.pexels.com/photos/376464/pexels-photo-376464.jpeg?cs=srgb&dl=pexels-ash-376464.jpg&fm=jpg" style={{height:"400px",padding:"20px"}}/>
  <Card.Body>
    <Card.Title>burger</Card.Title>
    <Card.Text>
     cost:400
    </Card.Text>
  </Card.Body>
  <Card.Footer>
  <Button
    color="primary"
  >
    Order now
  </Button>
  </Card.Footer>
</Card>
        </CardGroup>
      </div>
    </section>
  )
}

export default About;