import axios from 'axios';
import React, { useState,useEffect } from 'react';
import Cards from './Cards';
import '../App.js'

function Blog() {
  const [data,setData] = useState([]);
  useEffect(() =>{
    const fetchData =async () =>{
      try{
        let response = await axios.get("http://localhost:3001/fetch")
        console.log(response.data)
        setData(response.data)  
      }
      catch(err){
      }
    };
    fetchData()
  },[]);

  return (
  <div className='bg-black'>
    <br/>
    <br/>
    <h1 className='text-center text-white'>"TravEZ! Navigating Your Journey with Ease"</h1><br/>
    <p className='text-white mx-50px'>Greetings, fellow adventurers! Today, we're thrilled to introduce you to Travez, your new travel companion that's set to redefine the way you explore the world. Whether you're a seasoned globetrotter or a first-time traveler, Travez is designed to make your journeys seamless, enjoyable, and unforgettable.</p>
    <h1 className='text-info text-center text-black fs-10 text-white mt-2'>EXPLORE HERE</h1>
    <div className='container'>
    <div className='row mt-5'>{data.filter((e)=>(e.Img,e.Place,e.Country,e.Story)).map((e)=>(
      // <img src={e.Img} alt=""  className='w-25  rounded-5'/>
      <Cards data={e}/>
    ))}
  </div>  
  </div>
  </div>      
);
}
export default Blog;

  