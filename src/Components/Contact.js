import React, { useState } from 'react';
import axios from 'axios';
import { Button, Modal, ModalHeader, ModalBody,  Form, FormGroup, Label, Input,Row,Col, ModalFooter } from 'reactstrap';

function Contact(args) {
  const [modal, setModal] = useState(false);
  const [formdata,setFormdata] = useState({
    email:'',
    password:'',
    address:'',
    address2:'',
    city:'',
    state:'',
    zip:'',
    radio2:''
  })

  const handleInput = (e)=>{
    const {name,value} = e.target
    setFormdata({
      ...formdata,

      [name]:value
    })
  }

console.log(formdata)
 

  const toggle = (e) =>{ 
    e.preventDefault();
    setModal(!modal);}

  const handleSubmit = async (e) =>{
    e.preventDefault()
    try{
      let response = await axios.post("http://localhost:3001/batch58",formdata)
      console.log(response)
      alert("data inserted")
      alert("Registered Successfully")
      }
      catch (err){
        throw err
      }
     
    }

  return (
    <div>
      <Button color="success" onClick={toggle}>
        Sign Up
      </Button>
      <Modal isOpen={modal} toggle={toggle} {...args}>
        <ModalHeader toggle={toggle}>Modal title</ModalHeader>
        <ModalBody>
        <Form onSubmit={handleSubmit}>
  <Row>
    <Col md={6}>
      <FormGroup>
        <Label for="exampleEmail">
          Email
        </Label>
        <Input
          id="exampleEmail"
          name="email"
          placeholder="Email"
          type="email"
          value={formdata.email}
          onChange={handleInput}
          required
        />
      </FormGroup>
    </Col>
    <Col md={6}>
      <FormGroup>
        <Label for="examplePassword">
          Password
        </Label>
        <Input
          id="examplePassword"
          name="password"
          placeholder="password "
          type="password"
          pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}"
          value={formdata.password}
          onChange={handleInput}
          required
        />
      </FormGroup>
    </Col>
  </Row>
  <FormGroup>
    <Label for="exampleAddress">
      Address
    </Label>
    <Input
      id="exampleAddress"
      name="address"
      placeholder="1234 Main St"
      value={formdata.address}
      onChange={handleInput}
      required
    />
  </FormGroup>
  <FormGroup>
    <Label for="exampleAddress2">
      Address 2
    </Label>
    <Input
      id="exampleAddress2"
      name="address2"
      placeholder="Apartment, studio, or floor"
      value={formdata.address2}
      onChange={handleInput}
      required
    />
  </FormGroup>
  <Row>
    <Col md={6}>
      <FormGroup>
        <Label for="exampleCity">
          City
        </Label>
        <Input
          id="exampleCity"
          name="city"
          value={formdata.city}
          onChange={handleInput}
          required
        />
      </FormGroup>
    </Col>
    <Col md={4}>
      <FormGroup>
        <Label for="exampleState">
          State
        </Label>
        <Input
          id="exampleState"
          name="state"
          value={formdata.state}
          onChange={handleInput}
          required
        />
      </FormGroup>
    </Col>
    <Col md={2}>
      <FormGroup>
        <Label for="exampleZip">
          Zip
        </Label>
        <Input
          id="exampleZip"
          name="zip"
          value={formdata.zip}
          onChange={handleInput}
          required
        />
      </FormGroup>
    </Col>
  </Row>
  <legend className="col-form-label col-sm-2">
      Gender
    </legend>
    <Col sm={10}>
      <FormGroup check>
        <Input
          name="radio2"
          type="radio"
          value="Male"
          // checked={formdata==='Male'}
          onChange={handleInput}

        />
        {' '}
        <Label check>
          Male
        </Label>
      </FormGroup>
      <FormGroup check>
        <Input
          name="radio2"
          type="radio"
          value="Female"
          // checked={formdata==='Female'}
          onChange={handleInput}
        />
        {' '}
        <Label check>
          Female
        </Label>
      </FormGroup>
      </Col>
  <FormGroup check>
    <Input
      id="exampleCheck"
      name="check"
      type="checkbox"
    />
    <Label
      check
      for="exampleCheck"
    >
      Check me out
    </Label>
  </FormGroup>
  <a href='REgister for new user!'></a>
  <Button type="submit">
    Register
  </Button>
</Form>

        </ModalBody>
        
      </Modal>
    </div>
  );
}
export default Contact;