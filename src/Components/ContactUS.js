import React from 'react'
import {Form, FormGroup, Label, Input,Row,Col } from 'reactstrap';
function ContactUS() {
  return (
    <div className='bg-black'>
    <div className=' container text-center mt-5 '>
        <p className=' mt-5 fs-5 text-white'>Contact Us</p>
        <p className='mt-3 text-white'>  We'll get back to you as soon as travez@gmail.com</p>
    </div>
      <div className='mt-5'>
        <center>
             <Form style={{width:'50%'}}>
  <Row>
    <Col md={6}>
      <FormGroup>
        <Label for="exampleEmail"hidden>Name </Label>
        <Input
          name="name"
          placeholder="Name"
          type="text"
          required
        />
      </FormGroup>
    </Col>
    <Col md={6}>
      <FormGroup>
       
        <Input
          name="email"
          placeholder="Email"
          type="email"
          required
        />
      </FormGroup>
    </Col>
  </Row>
  <FormGroup>
   
    <Input
      name="phone"
      placeholder="Phone Number"
      required
    />
  </FormGroup>
  <FormGroup>
   
    <Input style={{height:'100px'}}
      name="message"
      placeholder="Message"
      required
          />
  </FormGroup>
  <p className='btn btn-secondary'>Send</p>
  </Form></center>
      
      </div>


    </div>
  )
}

export default ContactUS;