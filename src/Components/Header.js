import React from 'react'
import Login from './Login';
import {NavLink} from 'reactstrap';
import {NavDropdown ,Navbar, Container, Nav, Row, Col, Form, Button, } from 'react-bootstrap'

function Header() {
  return (
    <div>
    <Navbar expand="lg" className="bg-black text-white gap-5   ">
    <Container>
      <Navbar.Brand> <img src={0} alt='khan'/> </Navbar.Brand>
      <Navbar.Brand href="#home"  className=' text-white text-bold h-10px fs-20'>TravEZ</Navbar.Brand>
      <Navbar.Toggle aria-controls="basic-navbar-nav" />
      <Navbar.Collapse id="basic-navbar-nav">
        <div >
           <div className='d-flex justify-content-around'>
        <Nav className="me-auto bs-light text-white mx-5 gap-20">
          <Nav.Link href="home"  className=' text-white margin-left:5px'>Home</Nav.Link>
          <Nav.Link href="Blog"  className=' text-white'>Blog</Nav.Link>
          <Nav.Link href="About"  className=' text-white'>About</Nav.Link>
          <Nav.Link href="Contact"  className=' text-white'>Contact US</Nav.Link>
        </Nav>
          </div>
        </div>
      </Navbar.Collapse>
      <Form inline>
      <Row>
        <Col xs="auto">
          <Form.Control
            type="text"
            placeholder="Search"
            className=" mr-sm-2"
          />
        </Col>
        <Col xs="auto">
        {/* <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-search" viewBox="0 0 16 16">
          <path d="M11.742 10.344a6.5 6.5 0 1 0-1.397 1.398h-.001c.03.04.062.078.098.115l3.85 3.85a1 1 0 0 0 1.415-1.414l-3.85-3.85a1.007 1.007 0 0 0-.115-.1zM12 6.5a5.5 5.5 0 1 1-11 0 5.5 5.5 0 0 1 11 0"/>
          </svg> */}
          {/* <Button type="submit">Login</Button> */}
        </Col>
      </Row>
    </Form>
    <Nav className="ml-auto flex-column display:flex" navbar >
        <NavLink>
          <Login />
        </NavLink>
    </Nav> 
    </Container>
  </Navbar>
  </div>
  )
}

export default Header;