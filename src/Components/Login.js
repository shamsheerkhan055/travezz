
import React, { useState } from 'react';
import axios from 'axios';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter, Form, FormGroup,  Label, Input } from 'reactstrap';
import Contact from './Contact';


function Login(args) {
  const [modal, setModal] = useState(false);
  const [formdata, setFormdata] = useState({
    email:'',
    password:''
  })

  const handleInput = (e) =>{
    const {name,value} = e.target
    setFormdata({
        ...formdata,

        [name]:value
    })
  }

  console.log(formdata)

  const toggle = (e) => {
    e.preventDefault();
    setModal(!modal)};



    const handleSubmit = async (e) =>{
      e.preventDefault()
      try{
        let response = await axios.get(`http://localhost:3001/login/${formdata.Email}/${formdata.password}`)
        console.log(response)
      }
      catch (err){
        throw err
      }
     
     alert("login successfully")
     setModal(!modal)
    }



  return (
    <div>
      <Button color="light" onClick={toggle}>
        Login
      </Button>
      <Modal isOpen={modal} toggle={toggle} {...args}>
        <ModalHeader toggle={toggle}></ModalHeader>
        <ModalBody>
        <Form onSubmit={handleSubmit}>
  <FormGroup>
    {/* <Label
      for="exampleEmail">
      Email
    </Label> */}
    <Input
      id="exampleEmail"
      name="Email"
      placeholder="Email"
      type="email"
      value={formdata.Email}
      onChange={handleInput}
      required
    />
  </FormGroup>
  {' '}
  <FormGroup>
    {/* <Label
      for="examplePassword"
      
    >
      Password
    </Label> */}
    <Input
      id="examplePassword"
      name="password"
      placeholder="Password"
      pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}"
      type="password"
      value={formdata.password}
      onChange={handleInput}
      required
    />
  </FormGroup>
  {' '}
  <Button color='primary'>
    Login
  </Button>
</Form>
        </ModalBody>
        <ModalFooter>
          {/* <Button color="primary" onClick={toggle}> */}
            <Contact/>
          {/* </Button>{' '} */}
          <Button color="danger" onClick={toggle}>
            Cancel
          </Button>
        </ModalFooter>
      </Modal>
    </div>
  );
}

export default Login;