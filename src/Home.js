import './App.css';
import axios from 'axios';
import React, { useState,useEffect } from 'react';
import Cards from './Components/Cards';
import { Carousel} from 'react-bootstrap';
import { useNavigate } from 'react-router-dom';


function Home(args) {
  const [data,setData] = useState([]); 
  useEffect(() =>{
    const fetchData =async () =>{
      try{
        let response = await axios.get("http://localhost:3001/fetch")
        console.log(response.data)
        setData(response.data)  
      }
      catch(err){
      }
    };
    fetchData()
  },[]);
  return (
    <div className='bg-black mt-0'>
      <div>
      <Carousel className='fixed '>
    <Carousel.Item >
      <img src="https://www.keralaholidays.com/uploads/tourpackages-gallery/thumb/Kerala-Tour-with-Kodaikanal_(1).jpg" alt='khan' style={{width:'100%', height:'600px'}}/>
      <Carousel.Caption className='text-center fw-bolder'>
    
        <h3>Kodaikanal</h3>
        <p>Kodaikanal is a hill town in the southern Indian state of Tamil Nadu.</p>
      </Carousel.Caption>
    </Carousel.Item>
    <Carousel.Item>
    <img src="https://www.treebo.com/blog/wp-content/uploads/2022/12/Summer_news18.com_.jpg" style={{width:'100%', height:'600px'}}/>
      <Carousel.Caption><center>
        <h3>Munnar</h3>
        <p>Munnar is a town in the Western Ghats mountain range in India’s Kerala state.</p></center>
      </Carousel.Caption>
    </Carousel.Item>
    <Carousel.Item>
    <img src="https://www.tamilnadutourism.tn.gov.in/img/pages/large-desktop/kodaikanal-1655279477_0cdce0d4e58596e4fb33.webp" style={{width:'100%',height:'600px'}}/>
      <Carousel.Caption><center>
        <h3>Ooty</h3>
        <p>Ooty is a resort town in the Western Ghats mountains, in southern India's Tamil Nadu state.</p></center>
      </Carousel.Caption>
    </Carousel.Item>
    </Carousel>
    <br/>
    <br/>
    </div>
    
    <div>
          <h1 className='text-info text-center text-white fs-10'>DESTINATIONS</h1><span><ur/></span>
          <div className='container'>
          <div className='row mt-5'>{data.map((e)=>(
            // <img src={e.Img} alt=""  className='w-25  rounded-5'/>
            <Cards data={e}/>
          ))} 
        </div>  
        </div>
        </div>
    </div>
  );
}

export default Home;